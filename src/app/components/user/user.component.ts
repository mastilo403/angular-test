import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name:string = 'Zoki';
  age:number;
  email:string;
  address:Address;
  hobbies:string[];
  posts: Posts[];
  isEdit: boolean = false;

  constructor(private dataService:DataService) {
    console.log('konstruktor radi...');
  }

  ngOnInit() {
      console.log('ngOnInit radi...');
      this.name = 'Zoran Mastilović';
      this.age = 30;
      this.email = 'mastilo@gmail.com';
      this.address = {
        street: 'Ulica 1.maj',
        city: 'Niksic',
        state: 'Crna Gora',
      }
      this.hobbies = ['Write code', 'Watch movies', 'Listen to music'];
      this.dataService.getPosts().subscribe((posts) => {
        this.posts = posts;
        console.log(posts);
      });
  }

    onClick(){
      this.name = 'Zokic mali';
      this.hobbies.push('New Hobby');
    }

    addHobby(hobby){
      console.log(hobby);
      this.hobbies.unshift(hobby);
      return false;
    }
    deleteHobby(hobby){
    for(let i = 0; i < this.hobbies.length; i++){
      if(this.hobbies[i] == hobby){
        this.hobbies.splice(i, 1);
      }
    }
      console.log(hobby);
    }

    toggleEdit(){
    this.isEdit = !this.isEdit;
    }

}

interface Address{
    street:string,
    city:string,
    state:string
}

interface Posts{
  id: number,
    title: string,
    body: string,
    userId: number
}